<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model {
    protected $table = 'user';

    protected $allowedFields = ['username', 'password', 'firstname', 'lastname'];

    public function check($username, $password) {
        $this->where('username', $username);
        $query = $this->get();
        //return $this->getLastQuery(); //for debugging
        $row = $query->getRow();

       if ($row) { //check if SQL returned a row
            if (password_verify($password,$row->password)) { //verify password
                return $row;
            }
        }
       return null; //null will be returned, if there is no user
    }
}