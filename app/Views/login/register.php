<h3><?= $title ?></h3>
<form action="/login/registration">
    <div class="col-12">
    <?=\Config\Services::validation()->listErrors();?>
    </div>

    <div class="form-group">
    <label>Username</label>
    <input 
    class="form-control"
    name="user" 
    placeholder="Enter username" 
    max-length="30">
    </div>

    <div class="form-group">
    <label>First name</label>
    <input 
    class="form-control"
    name="firstname" 
    placeholder="Enter first name" 
    max-length="100">
    </div>

    <div class="form-group">
    <label>Last name</label>
    <input 
    class="form-control"
    name="lastname" 
    placeholder="Enter last name" 
    max-length="100">
    </div>

    <div class="form-group">
    <label>Password</label>
    <input 
    class="form-control"
    name="password" 
    type="password"
    placeholder="Enter password" 
    max-length="30">
    </div>

    <div class="form-group">
    <label>Password again</label>
    <input 
    class="form-control"
    name="confirmpassword" 
    type="password"
    placeholder="Enter password again" 
    max-length="30">
    </div>

    <button class="btn btn-primary">Submit</button>
</form>